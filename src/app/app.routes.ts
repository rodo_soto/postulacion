import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { Ejercicio1Component } from './components/ejercicio1/ejercicio1.component';
import { Ejercicio2Component } from './components/ejercicio2/ejercicio2.component';

const APP_ROUTES: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'ejercicio1', component: Ejercicio1Component},
  {path: 'ejercicio2', component: Ejercicio2Component},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);


