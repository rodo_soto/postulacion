import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../service/task.service';


@Component({
  selector: 'app-ejercicio2',
  templateUrl: './ejercicio2.component.html',
  styleUrls: ['./ejercicio2.component.css']
})
export class Ejercicio2Component implements OnInit {

  tasks: any;

  constructor(private taskService: TaskService) { }

  ngOnInit() {

this.getDatos2();

  }


  getDatos2(): void {

    this.taskService.getDatos2().
    subscribe( (tasks) => {

      console.log(tasks);
      if (tasks.success) {
        this.tasks = tasks;

      }
    });

  }
}
