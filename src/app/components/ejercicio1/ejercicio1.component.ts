import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../service/task.service';






@Component({
  selector: 'app-ejercicio1',
  templateUrl: './ejercicio1.component.html',
  styleUrls: ['./ejercicio1.component.css']
})
export class Ejercicio1Component implements OnInit {

  tasks: any;
  ordenArray: any;

  constructor(private taskService: TaskService) {
    this.ordenArray = [];
  }




  ngOnInit() {

  this.getDatos();
}

// Metodo que captura los datos del servicio

  getDatos(): void {

    this.taskService.getDatos().
    subscribe( (tasks) => {

      console.log(tasks);
      if (tasks.success) {
        this.tasks = tasks;
        this.orderArray();
      }
    });

  }
// metodo que retorna la cantidad de veces que se repite un valor
  counttask(task) {
    let  count = 0;
    this.tasks.data.forEach( e => {
      if (e === task) {
        count ++;
      }
    } );
    return count;
  }
// metodo que retorna la primera y ultima posicion de aparicion del valor dentro del arreglo
  getPosition(numero, position) {
  let firtPosition = 0;
  let lastPosition = 0;
  let fisrtPositionFlag = false;

  this.tasks.data.forEach((item, index) => {

    if (item === numero && !fisrtPositionFlag) {
firtPosition = index;
fisrtPositionFlag = true;
    }
    if (item === numero && index >= lastPosition) {
  lastPosition = index;
}

});

  if (position === 'last') {
  return lastPosition;
} else {
  return firtPosition;
}
  }
// metodo que ordena de mayor a menor el arreglo
  orderArray() {

    let output = [];
    let inserted;

    for (let i = 0, ii = this.tasks.data.length ; i < ii ; i++) {
      inserted = false;
      for (let j = 0, jj = output.length ; j < jj ; j++) {
        if (this.tasks.data[i] < output[j]) {
          inserted = true;
          output.splice(j, 0, this.tasks.data[i]);
          break;
        }
      }

      if (!inserted) {
        output.push(this.tasks.data[i]);
      }
      this.ordenArray = output;
    }

  }
  // metodo que colorea las filas deacuerdo al valor del numero del arreglo

  calculateStyles(tasks){

    let color: any;
    if (tasks === 1) {
      color = 'lightyellow';
    } else if (tasks < 1) {
      color = 'lightgray';
    } else if (tasks >= 2) {
      color = 'lightgreen';
    }

    return {'background-color': color};
  }

}



