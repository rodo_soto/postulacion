import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/Operators';
import { Task } from '../interfaces/task';
import { Observable } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private url1 = 'http://168.232.165.184/prueba/array';
  private url2 = 'http://168.232.165.184/prueba/dict';
  constructor(private http: HttpClient) {  }

  getDatos(): Observable<Task> {

  return this.http.get<Task>(this.url1);
  }
  getDatos2(): Observable<Task> {

    return this.http.get<Task>(this.url2);
    }


}
